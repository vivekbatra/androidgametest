package com.example.tappyspaceship01;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="DINO-RAINBOWS";

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    Item item;
    Item item2;
    Item item3;
    Item item4;
    Bitmap img;

    Player player;
    String movePlayer ="";


    int item1XPos = 100;
    int item1YPos = 50;

    int item2XPos = 100;
    int item2YPos = 250;

    int item3XPos = 100;
    int item3YPos = 450;

    int item4XPos = 100;
    int item4YPos =650;

    int lives = 3;
    int score = 0;




    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------

    // represent the TOP LEFT CORNER OF THE GRAPHIC

    // ----------------------------
    // ## GAME STATS
    // ----------------------------


    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        this.printScreenInfo();


        item = new Item(getContext(), item1XPos, item1YPos);

        img = BitmapFactory.decodeResource(context.getResources(), R.drawable.rainbow64);
        item.setImage(img);


        item2 = new Item(getContext(), item2XPos, item2YPos);
        img = BitmapFactory.decodeResource(context.getResources(), R.drawable.candy64);
        item2.setImage(img);

        item3 = new Item(getContext(), item3XPos, item3YPos);
        img = BitmapFactory.decodeResource(context.getResources(), R.drawable.poop64);
        item3.setImage(img);

        item4 = new Item(getContext(), item4XPos, item4YPos);
        img = BitmapFactory.decodeResource(context.getResources(), R.drawable.rainbow64);
        item4.setImage(img);

        player = new Player(getContext(), 1350, 400);
    }



    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        //@TODO: Start the player at the left side of screen
    }
    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    int numLoops = 0;

    public void updatePositions() {

        this.item.setxPosition(item.getxPosition() + 25);
        this.item.updateHitbox();
        this.item2.setxPosition(item2.getxPosition() + 25);
        this.item2.updateHitbox();
        this.item3.setxPosition(item3.getxPosition() + 25);
        this.item3.updateHitbox();
        this.item4.setxPosition(item4.getxPosition() + 25);
        this.item4.updateHitbox();



        if (movePlayer == "down")
        {
            if (this.player.getyPosition()+600 < screenHeight)
            {
                this.player.setyPosition(player.getyPosition() + 200);
                this.player.updateHitbox();
            }
            movePlayer = "";
        }
        else if (movePlayer == "up")
        {
            if (this.player.getyPosition() > 0)
            {
                this.player.setyPosition(player.getyPosition() - 200);
                this.player.updateHitbox();
            }
            movePlayer = "";
        }

        if (item.getHitbox().intersect(player.getHitbox()) == true || item2.getHitbox().intersect(player.getHitbox()) == true
                || item4.getHitbox().intersect(player.getHitbox()) == true)
        {
            score = score +1;
        }
        if (item3.getHitbox().intersect(player.getHitbox()) == true )
        {
            lives = lives -1;
        }

        if (lives == 0)
        {
            score = 0;
            lives = 3;
            this.item.setxPosition(item1XPos);
            this.item.updateHitbox();
            this.item2.setxPosition(item2XPos);
            this.item2.updateHitbox();
            this.item3.setxPosition(item3XPos);
            this.item3.updateHitbox();
            this.item4.setxPosition(item4XPos);
            this.item4.updateHitbox();
        }


        if (item.getxPosition() > screenWidth)
        {
            item.setxPosition(item1XPos);
            this.item.updateHitbox();
        }
        else if (item2.getxPosition() > screenWidth)
        {
            item2.setxPosition(item2XPos);
            this.item2.updateHitbox();
        }
        else if (item3.getxPosition() > screenWidth)
        {
            item3.setxPosition(item3XPos);
            this.item3.updateHitbox();
        }
        else if (item4.getxPosition() > screenWidth)
        {
            item4.setxPosition(item4XPos);
            this.item4.updateHitbox();
        }







    }



    public void spawnItem()
    {

        ArrayList<Integer> ids = new ArrayList<Integer>();
        ids.add(R.drawable.candy64);
        ids.add(R.drawable.poop64);
        ids.add(R.drawable.rainbow64);

        Collections.shuffle(ids);

        Bitmap gBall1 = BitmapFactory.decodeResource(getResources(), ids.get(0));

        Random r = new Random();
        int x = r.nextInt(1000) + 0;
        int y = r.nextInt(1000) + 0;
        Item i = new Item(getContext(), x,y);
        i.setImage(gBall1);


        canvas.drawBitmap(i.getImage(),i.getxPosition(),i.getyPosition(),paintbrush);


    }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.WHITE);


            paintbrush.setColor(Color.RED);
            this.canvas.drawLine(100,200, 1300, 200, paintbrush);
            this.canvas.drawBitmap(item.getImage(), item.getxPosition(), item.getyPosition(), paintbrush);
            canvas.drawRect(item.getHitbox(), paintbrush);



            this.canvas.drawLine(100,400, 1300, 400, paintbrush);
            this.canvas.drawBitmap(item2.getImage(), item2.getxPosition(), item2.getyPosition(), paintbrush);
            this.canvas.drawRect(item2.getHitbox(), paintbrush);



            this.canvas.drawLine(100,600, 1300, 600, paintbrush);
            this.canvas.drawBitmap(item3.getImage(), item3.getxPosition(), item3.getyPosition(), paintbrush);
            canvas.drawRect(item3.getHitbox(), paintbrush);



            this.canvas.drawLine(100,800, 1300, 800, paintbrush);
            this.canvas.drawBitmap(item4.getImage(), item4.getxPosition(), item4.getyPosition(), paintbrush);
            canvas.drawRect(item4.getHitbox(), paintbrush);




            this.canvas.drawBitmap(player.getImage(), player.getxPosition(), player.getyPosition(), paintbrush);
            canvas.drawRect(player.getHitbox(), paintbrush);




            // DRAW THE PLAYER HITBOX
            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);
            paintbrush.setTextSize(60);





            canvas.drawText("Lives: " + lives,
                    900,
                    880,
                    paintbrush
            );


            canvas.drawText("Score: " + score,
                    500,
                    880,
                    paintbrush
            );


            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(120);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------


    String fingerAction = "";

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            Float x = event.getX();
            Float y = event.getY();

            if (y > screenHeight/2)
            {
                Log.d(TAG, "*************** Clicked on x = " +x+ "y = " +y+ "***************");
                Log.d(TAG, "BOTTOM");
                this.movePlayer = "down";

            }
            else if (y < screenHeight/2)
            {
                Log.d(TAG, "*************** Clicked on x = " +x+ "y = " +y+ "***************");
                Log.d(TAG, "TOP");
                this.movePlayer = "up";
            }

        }
        else if (userAction == MotionEvent.ACTION_UP) {

        }

        return true;
    }
}
